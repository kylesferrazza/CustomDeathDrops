package io.github.tubakyle.customdeathdrops;

import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by Kyle on 7/15/2014.
 */
public class CustomDeathDrops extends JavaPlugin{
    public void onEnable() {
        saveDefaultConfig();
        getServer().getPluginManager().registerEvents(new EventListener(this), this);
    }
}
