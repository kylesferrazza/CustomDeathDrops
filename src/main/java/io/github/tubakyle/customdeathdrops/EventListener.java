package io.github.tubakyle.customdeathdrops;

import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by Kyle on 7/15/2014.
 */
public class EventListener implements Listener {

    private final JavaPlugin plugin;

    public EventListener(CustomDeathDrops plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onDeath(PlayerDeathEvent e) {
        Player p = e.getEntity();
        if (!(p.hasPermission("cdd.ignore"))) {
            FileConfiguration cfg = plugin.getConfig();
            plugin.reloadConfig();
            String item = cfg.getString("item-to-drop");
            int amount = cfg.getInt("amount");
            e.getDrops().clear();
            e.getDrops().add(new ItemStack(Material.getMaterial(item), amount));
        }
    }
}
