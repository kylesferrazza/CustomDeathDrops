# CustomDeathDrops
This plugin changes what a player drops on their death.
<br> If a player does not have the permission
<pre> cdd.ignore </pre>
, instead of their inventory contents being dropped on their death, a user configurable item stack will drop.
 <br> This can be configured from the config.yml once the plugin is installed.
 
## Config
The config is layed out as follows:
<pre>
item-to-drop:
amount:
</pre>
These are pretty self explanatory.
<br>A list of compatible item names can be found <a href="http://jd.bukkit.org/rb/apidocs/org/bukkit/Material.html">here</a>.

For example, if your config file contains:
<pre>
item-to-drop: 'DIRT'
amount: 64
</pre>
, every player without the permission <pre> cdd.ignore </pre> will drop a stack of 64 dirt on their death, instead of their inventory.

## Permissions
If a user has the permission
<pre> cdd.ignore </pre>
, the plugin will completely ignore them, and on their death, their inventory will drop as usual.